package racecar;

public class Race {
    private int amountOfCars = 0;
    private Racecar[] arrayOfCars;
    private double length = 0.0;

    public Race(double length, Racecar... cars) {
        this.amountOfCars = cars.length;
        this.length = length;
        arrayOfCars = cars;
    }

    public void addRacecar(Racecar car) {
        if (amountOfCars == arrayOfCars.length) {
            Racecar[] newArrayOfCars = new Racecar[amountOfCars * 2];
            for (int i =  0; i < amountOfCars; i++) {
                newArrayOfCars[i] = arrayOfCars[i];
            }
            arrayOfCars = newArrayOfCars;
            arrayOfCars[amountOfCars] = car;
            amountOfCars++;
        } else {
            arrayOfCars[amountOfCars] = car;
        }
    }



    public Racecar calculateWinner() {
        Racecar[] arrayOfWinnerCars = new Racecar[amountOfCars];
        int amountOfWinnerCars = 0;


        for (Racecar e : arrayOfCars) {
            if (e != null && e.getMileage() >= length) {
                arrayOfWinnerCars[amountOfWinnerCars] = e;
                amountOfWinnerCars++;
            }
        }
        if (arrayOfWinnerCars[0] != null) {
            Racecar winnerCar = arrayOfWinnerCars[0];
            for (int i = 0; i < amountOfWinnerCars; i++) {
                if (arrayOfWinnerCars[i].getMileage() > winnerCar.getMileage()) {
                    winnerCar = arrayOfWinnerCars[i];
                }
            }
            return winnerCar;
        } else {
            return null;
        }
    }

    public void step() {
        for (Racecar e : arrayOfCars) {
            if (e != null) {
                e.driving();
            }
        }
    }

    public Racecar execute() {
        Racecar winner = null;
        while (winner == null) {
            step();
            winner = calculateWinner();
        }
        return winner;
    }
}
