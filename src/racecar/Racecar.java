package racecar;

public class Racecar {
    private String driver = "";
    private CarType carType;
    private int maxSpeed = 0;
    private double mileage = 0.0;

    public Racecar(String driver, CarType carType, int maxSpeed) {
        this.driver = driver;
        this.carType = carType;
        this.maxSpeed = maxSpeed;
    }

    public void driving() {
        mileage += maxSpeed * Math.random();
    }

    public void output() {
        System.out.println("Driver: " + driver);
        System.out.println("Car Type: " + carType);
        System.out.println("Max Speed: " + maxSpeed);
        System.out.println("Mileage: " + mileage);
    }
    
    public double getMileage() {
        return mileage;
    }
    
    public String getName() {
        return driver;
    }
}
