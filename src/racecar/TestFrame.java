package racecar;

public class TestFrame {

    public static void main(String[] args) {
        Racecar car1 = new Racecar("A", CarType.RACECAR, 180);
        Racecar car2 = new Racecar("B", CarType.RACECAR, 181);
        Racecar car3 = new Racecar("C", CarType.SUPERRACECAR, 200);
        Racecar car4 = new Racecar("D", CarType.SUPERDUPERRACECAR, 220);

        Racecar[] cars = new Racecar[] {car1, car2, car3, car4};

        Race race = new Race(1337.42, cars);
        
        Racecar car5 = new Racecar("E", CarType.SUPERRACECAR, 210);
        Racecar car6 = new Racecar("F", CarType.SUPERRACECAR, 215);
        race.addRacecar(car5);
        race.addRacecar(car6);
        
        race.execute().output();

    }

}
